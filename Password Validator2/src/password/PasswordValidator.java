package password;

import java.util.regex.Pattern;

/*
 * Kashish Sachdeva - 991541815
 * Validates password and is developed using TDD
 * return true if the length is more than or equal to 8. fails for a space.
 * */
public class PasswordValidator {
	private static int MIN_LENGTH = 8;
	public static boolean isValidDigitsLength(String password) {
		return Pattern.compile("\\d{2,}+").matcher(password).find();
	}
	
	
	
	
	public static boolean isValidLength( String password ) {
		return password.indexOf(" ") < 0 && password.length() >= MIN_LENGTH;
		//this(first cond) is true when it dont have spaces
		//&& - if the first exp is false, it doesn't bother checking this one
//		if ( password.indexOf(" ") >= 0 ) {
//			return false;
//		}
//		return password.length() >= MIN_LENGTH;
		//		if( password.length() >= MIN_LENGTH) {
//			return true;
//		}
//		else {
//			return false;
//		}
		
	}
}
