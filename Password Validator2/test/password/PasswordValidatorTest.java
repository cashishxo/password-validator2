package password;

import static org.junit.Assert.*;

import org.junit.Test;
/*
 * Kashish Sachdeva - 991541815 
 * */
public class PasswordValidatorTest {
	@Test
	public void testIsValidDigitsLengthRegular() {
		boolean result = PasswordValidator.isValidDigitsLength("123ksksksk");
		assertTrue("Invalid Number of Digits ",result);
	}
	@Test
	public void testIsValidDigitsLengthException() {
		boolean result = PasswordValidator.isValidDigitsLength("sjdflk");
		assertFalse("Invalid Number of digits", result);
	}
	@Test
	public void testIsValidDigitsLengthBoundaryIn() {
		boolean result = PasswordValidator.isValidDigitsLength("22ksljdf");
		assertTrue("Valid number of digits", result);
	}
//	kashish
	@Test
	public void testIsValidDigitsLengthBoundaryOut() {
		boolean result = PasswordValidator.isValidDigitsLength("1ldjf");
		assertFalse("Failed", result);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	@Test
	public void testIsValidLengthRegular() {
		boolean result = PasswordValidator.isValidLength( "1234567890" );
		assertTrue( "Invalid length", result);
	}
	@Test
	public void testIsValidLengthException() {
		boolean result = PasswordValidator.isValidLength( "" );
		assertFalse( "Invalid length", result);
	
	}
	@Test
	public void testIsValidExceptionSpaces() {
		boolean result = PasswordValidator.isValidLength( "    test ye   " );
		assertFalse( "Invalid length", result);
	}
	
	
	
	
	
	

	/*Kashish Sachdeva*/
	@Test
	public void testIsValidBoundaryIn() {
		boolean result = PasswordValidator.isValidLength( "12345678" );
		assertTrue( "Invalid length", result);
	}

	
	/*Kashish Sachdeva*/
	@Test
	public void testIsValidBoundaryOut() {
		boolean result = PasswordValidator.isValidLength( "1234567" );
		assertFalse( "Invalid length", result);
	}
	
	
	
	
	
	
	
}

